package com.punicapp.sample;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Pair;

import com.google.common.base.Optional;
import com.google.gson.reflect.TypeToken;
import com.punicapp.rxrepolocal.LocalRepository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LocalRepoInstrumentedTest {
    private static class TestClass {
        int a;
        String b;
        boolean c;

        public TestClass(int a, String b, boolean c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TestClass testClass = (TestClass) o;

            if (a != testClass.a) return false;
            if (c != testClass.c) return false;
            return b != null ? b.equals(testClass.b) : testClass.b == null;
        }

        @Override
        public int hashCode() {
            int result = a;
            result = 31 * result + (b != null ? b.hashCode() : 0);
            result = 31 * result + (c ? 1 : 0);
            return result;
        }
    }

    @Test
    public void testSaveGet() throws Exception {
        try {
            Context appContext = InstrumentationRegistry.getTargetContext();
            final LocalRepository repository = new LocalRepository(appContext);
            repository.clear().subscribe();

            Observable.fromIterable(Arrays.asList(
                    new Pair<>("qwe", 5),
                    new Pair<>("asd", "zxc"),
                    new Pair<>("zxc", Arrays.asList(5, 6, 7)),
                    new Pair<>("sdf", new TestClass(5, "b", true))
            )).flatMap(new Function<Pair<String, ?>, Observable<Object>>() {
                @Override
                public Observable<Object> apply(Pair<String, ?> stringPair) throws Exception {
                    return repository.save(stringPair.first, stringPair.second).toObservable();
                }
            }).subscribe();

            Assert.assertFalse(repository.instantHas("wer"));
            repository.instantSave("wer", 75);
            Assert.assertEquals((int) repository.<Integer>instantGet("wer", Integer.class).or(0), 75);

            class EntryInfo {
                String key;
                Type type;
                Object expectedResult;

                public EntryInfo(String key, Type type, Object expectedResult) {
                    this.key = key;
                    this.type = type;
                    this.expectedResult = expectedResult;
                }
            }

            Observable.fromArray(
                    new EntryInfo("qwe", Integer.class, 5),
                    new EntryInfo("qwe", String.class, "5"),
                    new EntryInfo("asd", String.class, "zxc"),
                    new EntryInfo("zxc", new TypeToken<List<Integer>>() {
                    }.getType(), Arrays.asList(5, 6, 7)),
                    new EntryInfo("wer", Double.class, 75.),
                    new EntryInfo("sdf", TestClass.class, new TestClass(5, "b", true))
            ).flatMap(new Function<EntryInfo, ObservableSource<Pair<EntryInfo, Object>>>() {
                @Override
                public ObservableSource<Pair<EntryInfo, Object>> apply(EntryInfo entryInfo) throws Exception {
                    return repository.get(entryInfo.key, entryInfo.type).zipWith(Single.just(entryInfo),
                            new BiFunction<Object, EntryInfo, Pair<EntryInfo, Object>>() {
                                @Override
                                public Pair<EntryInfo, Object> apply(Object o, EntryInfo entryInfo) throws Exception {
                                    return new Pair<>(entryInfo, ((Optional) o).orNull());
                                }
                            }).toObservable();
                }
            }).doOnNext(new Consumer<Pair<EntryInfo, Object>>() {
                @Override
                public void accept(Pair<EntryInfo, Object> entryInfoObjectPair) throws Exception {
                    Assert.assertEquals(entryInfoObjectPair.second, entryInfoObjectPair.first.expectedResult);
                }
            }).subscribe();
        } catch (Exception e) {
            Assert.fail("Exception not handling! " + e.toString());
        }
    }

    @Test
    public void testMutlithreaded() throws Exception {
        try {
            Context appContext = InstrumentationRegistry.getTargetContext();
            final LocalRepository localRepository = new LocalRepository(appContext);
            localRepository.clear().subscribe();

            final int itemsPerThread = 1000;
            int threads = 3;

            List<Observable<String>> observables = new ArrayList<>(threads);

            for (int i = 0; i < threads; ++i) {
                final int finalI = i;
                Observable<String> observable = Observable.create(new ObservableOnSubscribe<Pair<String, String>>() {
                    @Override
                    public void subscribe(ObservableEmitter<Pair<String, String>> observableEmitter) throws Exception {
                        for (int j = 0; j < itemsPerThread; ++j) {
                            String key = String.valueOf(finalI * itemsPerThread + j);
                            observableEmitter.onNext(new Pair<String, String>(key, key + "asd"));
                        }
                        observableEmitter.onComplete();
                    }
                }).flatMap(new Function<Pair<String, String>, ObservableSource<String>>() {
                    @Override
                    public ObservableSource<String> apply(Pair<String, String> stringStringPair) throws Exception {
                        return localRepository.save(stringStringPair.first, stringStringPair.second).toObservable();
                    }
                }).subscribeOn(Schedulers.newThread());
                observables.add(observable);
            }

            Observable.merge(observables).blockingSubscribe();

            for (int i = 0; i < itemsPerThread * threads; ++i) {
                String key = String.valueOf(i);
                String value = localRepository.<String>instantGet(key, String.class).orNull();
                Assert.assertEquals(key + "asd", value);
            }
        } catch (Exception e) {
            Assert.fail("Exception not handling! " + e.toString());
        }
    }
}