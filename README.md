# gradle-simple

[![](https://jitpack.io/v/org.bitbucket.punicappinternal/rxRepoLocal.svg)](https://jitpack.io/#org.bitbucket.punicappinternal/rxRepoLocal)

Punicapp Repo Realm library, which is used in internal projects.

To install the library add: 
 
```
   allprojects {
       repositories {
           maven { url 'https://jitpack.io' }
       }
   }
   dependencies {
           compile 'org.bitbucket.punicappinternal:rxRepoRealm:1.0.1'
   }
```

Under [MIT LICENSE](https://bitbucket.org/punicappinternal/rxrepolocal/src/develop/LICENSE)