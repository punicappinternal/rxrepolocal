package com.punicapp.rxrepolocal

import android.content.Context
import android.content.SharedPreferences
import com.google.common.base.Optional
import com.google.gson.Gson
import com.punicapp.rxrepocore.IKeyValueRepository
import io.reactivex.Single
import io.reactivex.functions.Consumer
import io.reactivex.functions.Function
import java.lang.reflect.Type

class LocalRepository(context: Context) : IKeyValueRepository<String> {
    private val prefs: SharedPreferences

    init {
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    override fun <V> saveInChain(key: String): Consumer<V> {
        return Consumer { o ->
            val success = prefs.edit()
                    .putString(key, gson.toJson(o))
                    .commit()
            if (!success)
                throw RuntimeException("Couldn't save prefs \"" + PREFS_NAME + "\"")
        }
    }

    override fun removeInChain(): Consumer<String> {
        return Consumer { s ->
            val success = prefs.edit()
                    .remove(s)
                    .commit()
            if (!success)
                throw RuntimeException("Couldn't save prefs \"" + PREFS_NAME + "\"")
        }
    }

    override fun <V> getInChain(t: Type): Function<String, Optional<V>> {
        return Function { s ->
            val v = gson.fromJson<V>(prefs.getString(s, null), t)
            Optional.fromNullable(v)
        }
    }

    override fun hasInChain(key: String): Function<String, Boolean> {
        return Function { s -> prefs.contains(s) }
    }

    override fun clear(): Single<Boolean> {
        return Single.create { singleEmitter -> singleEmitter.onSuccess(prefs.edit().clear().commit()) }
    }

    companion object {
        private val PREFS_NAME = "prefs"
        private val gson = Gson()
    }
}
